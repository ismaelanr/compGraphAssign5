#version 330 core

layout(location=0) out vec4 vFragColor;	//fragment shader output

in vec3 thePosition;
in vec3 theNormal;
in vec3 theColor;

uniform vec3 LightPosition;
uniform vec3 Ambient; 
uniform vec3 EyePosition; //for specular

void main()
{
	//all calculations done in the fragment shader or smoother shading
	vec3 lightVector = normalize(LightPosition - thePosition);

	//diffuse
	float bright = dot(lightVector, theNormal);
	vec4 diffuse = vec4(bright, bright, bright, 1.0);

	//ambient
	vec3 ambientpluscolor = theColor * Ambient;

	//specular
	vec3 reflectedLightVector = reflect(-lightVector, theNormal);
	vec3 eyeVector = normalize(EyePosition - thePosition);
	float spec = dot(reflectedLightVector, eyeVector);
	spec = pow(spec, 80);
	vec4 specular = vec4(spec, spec, spec, 1.0);
	vFragColor = vec4(ambientpluscolor,1.0) + clamp(diffuse, 0, 1) + clamp(specular,0,1);
}