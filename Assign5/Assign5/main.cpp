
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <iostream>
#include <vector>

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> 
#include <glm/gtx/transform.hpp>

#include "GLSLShader.cpp"
#define GLEW_STATIC

#include "GLSLShader.h"

#define GL_CHECK_ERRORS assert(glGetError()== GL_NO_ERROR);

#ifdef _DEBUG 
#pragma comment(lib, "glew_x86_d.lib")
#pragma comment(lib, "freeglut_x86_d.lib")
#else
#pragma comment(lib, "glew_x86_r.lib")
#pragma comment(lib, "freeglut_static_x86.lib")
#endif


using namespace std;



//screen size
const int WIDTH  = 1280;
const int HEIGHT = 960;

//shader reference
GLSLShader shader;

//vertex array and vertex buffer object IDs
GLuint vaoID;
GLuint vboVerticesID;
GLuint vboIndicesID;

//out vertex struct for interleaved attributes
struct Vertex {
  glm::vec3 position;
  glm::vec3 color;
  glm::vec3 normal;
};

//Circles
Vertex vertices[5740];
GLushort indices[5740];

//Probably won't need all these arrays
//Vertex vertices2[1600];
//GLushort indices2[1600];

//Cylinder
//Vertex cylinderVert[1010];
//GLushort cylinderInd[1010];

//torus 
//Vertex torusVert[1530];
//GLushort torusInd[1530];

//Elipsoid
//vector<Vertex> elipsoidVert;
//vector<GLushort> elipsoidInd;





//Vertex vertices[3];
//GLushort indices[3];

//projection and modelview matrices
enum { Xaxis = 0, Yaxis = 1, Zaxis = 2, NumAxes = 3 };
int Axis = Yaxis;
GLfloat Theta[NumAxes] = { 0.0, 0.0, 0.0 };

glm::vec3 myCylRot(4.5, 3.0, 3.0);
glm::vec3 mySphereRot(0.0, 0.0, -2.95);
glm::vec3 myTorRot(-3.9, -3.0, -3.0);
glm::vec3 myElRot(-4.9, -5.0, -4.0);

glm::mat4  P = glm::perspective(glm::radians(45.0f), (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f);
glm::mat4  M = glm::mat4(1);
glm::mat4  M1 = glm::mat4(1);
glm::mat4  M2 = glm::mat4(1);
glm::mat4  M3 = glm::mat4(1);
glm::mat4  V = glm::lookAt(glm::vec3(20,3,20), // Camera is at (4,3,3), in World Space
			   glm::vec3(0,0,0), // and looks at the origin
			   glm::vec3(1,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
			   );


//OpenGL initialization
void OnInit()
{
  GL_CHECK_ERRORS
    //load the shader
    shader.LoadFromFile(GL_VERTEX_SHADER, "shaders/shader.vert");
  shader.LoadFromFile(GL_FRAGMENT_SHADER, "shaders/shader.frag");
  //compile and link shader
  shader.CreateAndLinkProgram();
  shader.Use();
  //add attributes and uniforms
  shader.AddAttribute("vVertex");
  shader.AddAttribute("vColor");
  shader.AddUniform("MVP");
  shader.UnUse();

  GL_CHECK_ERRORS

    //sphere	
    const float PI = 3.141592f;
  GLfloat setX = 0.0, setY = 0.0, setZ = -2.95, x, y, z, alpha, beta; // Storage for coordinates and angles
  GLfloat radius = 2.0f;
  int gradation = 20; //Amount of Samples taken

  int count = 0;
    
	
  for (alpha = 0.1; alpha < radius*PI; alpha += PI / gradation)
    {	
      for (beta = 0.1; beta < radius*PI; beta += PI / gradation)
	{
	  indices[count] = count;
		
	  x = radius*cos(beta)*sin(alpha) + setX;
	  y = radius*sin(beta)*sin(alpha) + setY;
	  z = radius*cos(alpha) - setZ;

	  vertices[count].position = glm::vec3(x,y,z);
	  vertices[count].color = glm::vec3(0.0, 1.0, 0.0);

	  vertices[count].normal = glm::normalize(vertices[count].position);

	  count++;
        }
    }

  //cylinder
  int numMajor = 10,numMinor = 100;
  float height = 5.0, rad = 1.5;
  double majorStep = height / numMajor;
  double minorStep = 2.0 * PI / numMinor;
  int i, j;

  for (i = 0; i < numMajor; ++i)
    {
      GLfloat z0 = 0.5 * height - i * majorStep+3;
      GLfloat z1 = z0 - majorStep;

      for (j = 0; j <= numMinor; ++j)
	{
	  double a = j * minorStep;
	  GLfloat x1 = rad * cos(a)+3;
	  GLfloat y1 = rad * sin(a)+3;
	  //glNormal3f(x / rad, y / rad, 0.0);
	  //glTexCoord2f(j / (GLfloat) numMinor, i / (GLfloat) numMajor);
	  //glVertex3f(x, y, z0);
	  indices[count] = count;

	  vertices[count].position = glm::vec3(x1,y1,z0);
	  vertices[count].color = glm::vec3(0.0, 0.0, 1.0);

	  vertices[count].normal = glm::normalize(vertices[count].position);
	
	  count++;
	}
    }

  //torus
  int numc = 15, numt = 50;
  int l, m, n;
  double s, t, x2, y2, z2, twopi;

  twopi = 2 * PI;
  for (l = 0; l < numc; l++)
    {
      for (m = 0; m <= numt; m++)
        {
	  for (n = 1; n >= 0; n--)
            {
	      s = (l + n) % numc + 0.5;
	      t = m % numt;

	      x2 = (1 + .1*cos(s*twopi / numc))*cos(t*twopi / numt)-3;
	      y2 = (1 + .1*cos(s*twopi / numc))*sin(t*twopi / numt)-3;
	      z2 = .1 * sin(s * twopi / numc)-3;
		
	      indices[count] = count;
	      vertices[count].position = glm::vec3(x2,y2,z2);
	      vertices[count].color = glm::vec3(0.0, 1.0, 1.0);
		
	      vertices[count].normal = glm::normalize(vertices[count].position);

	      count++;
	      //glVertex3f(x, y, z);
            }
        }
    }

  //ellipsoid
  GLfloat myX = 0.0, myY = 0.0, myZ = -2.95, x3, y3, z3, alph, bet; // Storage for coordinates and angles
  GLfloat radi = 2.0f;
  int grad = 20; //Amount of Samples taken

  for (alph = 0.1; alph < radi*PI; alph += PI / gradation)
    {	
      for (bet = 0.1; bet < radi*PI; bet += PI / grad)
	{
	  x3 = (radi*cos(bet)*sin(alph) + myX-8)*.60;
	  y3 = radi*sin(bet)*sin(alph) + myY-5;
	  z3 = radi*cos(alph) - myZ-5;
		
	  indices[count] = count;

	  vertices[count].position = glm::vec3(x3,y3,z3);
	  vertices[count].color = glm::vec3(1.0, 1.0, 0.0);

	  vertices[count].normal = glm::normalize(vertices[count].position);

	  count++;
        }
    }
cout << vertices[count].normal.x << endl;

  //setup triangle geometry
  //setup triangle vertices
  /*vertices[0].color=glm::vec3(1,0,0);
    vertices[1].color=glm::vec3(0,1,0);
    vertices[2].color=glm::vec3(0,0,1);

    vertices[0].position=glm::vec3(-1,-1,0);
    vertices[1].position=glm::vec3(0,1,0);
    vertices[2].position=glm::vec3(1,-1,0);

    //setup triangle indices
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
  */
	
	
  GL_CHECK_ERRORS


    glGenVertexArrays(1, &vaoID);
  glGenBuffers(1, &vboVerticesID);
  glGenBuffers(1, &vboIndicesID);
  GLsizei stride = sizeof(Vertex);

  glBindVertexArray(vaoID);

  glBindBuffer (GL_ARRAY_BUFFER, vboVerticesID);
  //pass triangle vertices to buffer object
  glBufferData (GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);
  GL_CHECK_ERRORS
    //enable vertex attribute array for position
    glEnableVertexAttribArray(shader["vVertex"]);
  glVertexAttribPointer(shader["vVertex"], 3, GL_FLOAT, GL_FALSE,stride,0);
  GL_CHECK_ERRORS
    //enable vertex attribute array for colour
    glEnableVertexAttribArray(shader["vColor"]);
  glVertexAttribPointer(shader["vColor"], 3, GL_FLOAT, GL_FALSE,stride, (const GLvoid*)offsetof(Vertex, color));
  GL_CHECK_ERRORS
    //pass indices to element array buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndicesID); //glVertex3f
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices[0], GL_STATIC_DRAW);
  GL_CHECK_ERRORS 



    cout<<"Initialization successfull"<<endl;
}

//release all allocated resources
void OnShutdown() {
  //Destroy shader
  shader.DeleteShaderProgram();

  //Destroy vao and vbo
  glDeleteBuffers(1, &vboVerticesID);
  glDeleteBuffers(1, &vboIndicesID);
  glDeleteVertexArrays(1, &vaoID);

  cout<<"Shutdown successfull"<<endl;
}

//resize event handler
void OnResize(int w, int h) {
  //set the viewport size
  glViewport (0, 0, (GLsizei) w, (GLsizei) h);
  //setup the projection matrix
  //P = glm::ortho(-1,1,-1,1);

	
	
}

//display callback function
void OnRender() {

  //clear the colour and depth buffer
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  //bind the shader
  shader.Use();
  //pass the shader uniform
  glUniformMatrix4fv(shader("MVP"), 1, GL_FALSE, glm::value_ptr(P*V*M));
  glDrawArrays(GL_TRIANGLE_FAN,0,1600);//draw sphere FAN
  shader.UnUse();

  shader.Use();
  glUniformMatrix4fv(shader("MVP"), 1, GL_FALSE, glm::value_ptr(P*V*M1));
  glDrawArrays(GL_TRIANGLE_FAN,1601,1009);//draw cylinder FAN?STRIP?	
  shader.UnUse();
  
  shader.Use();
  glUniformMatrix4fv(shader("MVP"), 1, GL_FALSE, glm::value_ptr(P*V*M2));
  glDrawArrays(GL_TRIANGLE_STRIP, 2610, 1530); //draw torus STRIP
  shader.UnUse();

  shader.Use();
  glUniformMatrix4fv(shader("MVP"), 1, GL_FALSE, glm::value_ptr(P*V*M3));
  glDrawArrays(GL_TRIANGLE_FAN, 4140, 1600); //draw ellipsoid FAN
  shader.UnUse();
	 
  //swap front and back buffers to show the rendered result
  glutSwapBuffers();
}

void idle()
{
  Theta[Axis] += 0.01;
  if ( Theta[Axis] > 360.0 ) {
    Theta[Axis] -= 360.0;
  }
  M = glm::rotate(Theta[Axis], mySphereRot);
  M1 = glm::rotate(Theta[Axis], myCylRot);
  M2 = glm::rotate(Theta[Axis], myTorRot);
  M3 = glm::rotate(Theta[Axis], myElRot);
  glutPostRedisplay();
}

int main(int argc, char** argv) {


  //freeglut initialization calls
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  glutInitContextVersion (3, 3);
  glutInitContextFlags (GLUT_CORE_PROFILE | GLUT_DEBUG);
  glutInitWindowSize(WIDTH, HEIGHT);
  glutCreateWindow("Simple triangle - OpenGL 3.3");

  //glew initialization
  glewExperimental = GL_TRUE;
  GLenum err = glewInit();
  if (GLEW_OK != err)	{
    cerr<<"Error: "<<glewGetErrorString(err)<<endl;
  } else {
    if (GLEW_VERSION_3_3)
      {
	cout<<"Driver supports OpenGL 3.3\nDetails:"<<endl;
      }
  }
  err = glGetError(); //this is to ignore INVALID ENUM error 1282
  GL_CHECK_ERRORS

    //print information on screen
    cout<<"\tUsing GLEW "<<glewGetString(GLEW_VERSION)<<endl;
  cout<<"\tVendor: "<<glGetString (GL_VENDOR)<<endl;
  cout<<"\tRenderer: "<<glGetString (GL_RENDERER)<<endl;
  cout<<"\tVersion: "<<glGetString (GL_VERSION)<<endl;
  cout<<"\tGLSL: "<<glGetString (GL_SHADING_LANGUAGE_VERSION)<<endl;

  GL_CHECK_ERRORS

    //opengl initialization
    OnInit();

  //callback hooks
  glutCloseFunc(OnShutdown);
  glutDisplayFunc(OnRender);
  glutReshapeFunc(OnResize);
  glutIdleFunc(idle);

  //main loop call
  glutMainLoop();

  return 0;
}


