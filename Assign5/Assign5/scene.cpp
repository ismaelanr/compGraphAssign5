
#define _USE_MATH_DEFINES
#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#endif

#include <stdlib.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4, glm::ivec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <glm/gtx/color_space.hpp>
#include <vector>
#include <cmath>
#include <math.h>

 char choice;
 bool isOn = false; 


static void sphere(float setX, float setY, float setZ)
{

    
    const float PI = 3.141592f;
    GLfloat x, y, z, alpha, beta; // Storage for coordinates and angles
    GLfloat radius = 2.0f;
    int gradation = 20; //Amount of Samples taken

    for (alpha = 0.0; alpha < radius*PI; alpha += PI / gradation)
    {	
        //LINE_LOOP for wire
        //TRIANGLE_STRIP for fill
        glColor3f(1.0, 1.0, 0.0);

        glBegin(GL_LINE_LOOP);
        for (beta = 0.0; beta < radius*PI; beta += PI / gradation)
        {
            x = radius*cos(beta)*sin(alpha) + setX;
            y = radius*sin(beta)*sin(alpha) + setY;
            z = radius*cos(alpha) - setZ;
            glVertex3f(x, y, z);
            x = radius*cos(beta)*sin(alpha + PI / gradation) + setX;
            y = radius*sin(beta)*sin(alpha + PI / gradation) + setY;
            z = radius*cos(alpha + PI / gradation) - setZ;
            glVertex3f(x, y, z);
        }
        glEnd();
    }
}
static void sphereFilled(float setX, float setY, float setZ)
{

    
    const float PI = 3.141592f;
    GLfloat x, y, z, alpha, beta; // Storage for coordinates and angles
    GLfloat radius = 2.0f;
    int gradation = 20; //Amount of Samples taken

  
    for (alpha = 0.0; alpha < radius*PI; alpha += PI / gradation)
    {	
        //LINE_LOOP for wire
        //TRIANGLE_STRIP for fill
        
        glColor3f(1.0, 1.0, 0.0);
        glBegin(GL_QUAD_STRIP);
        for (beta = 0.0; beta < radius*PI; beta += PI / gradation)
        {
            x = radius*cos(beta)*sin(alpha) + setX;
            y = radius*sin(beta)*sin(alpha) + setY;
            z = radius*cos(alpha) - setZ;
            glVertex3f(x, y, z);
            x = radius*cos(beta)*sin(alpha + PI / gradation) + setX;
            y = radius*sin(beta)*sin(alpha + PI / gradation) + setY;
            z = radius*cos(alpha + PI / gradation) - setZ;
            glVertex3f(x, y, z);
        }
        glEnd();
    }
}

static void torus(int numc, int numt)
{
    int i, j, k;
    double s, t, x, y, z, twopi;
    float PI = 3.141592f;

    twopi = 2 * PI;
    for (i = 0; i < numc; i++)
    {
        glBegin(GL_LINE_LOOP);
        glColor3f(0.0f, 1.0f, 1.0f);
        for (j = 0; j <= numt; j++)
        {
            for (k = 1; k >= 0; k--)
            {
                s = (i + k) % numc + 0.5;
                t = j % numt;

                x = (1 + .1*cos(s*twopi / numc))*cos(t*twopi / numt);
                y = (1 + .1*cos(s*twopi / numc))*sin(t*twopi / numt);
                z = .1 * sin(s * twopi / numc);
                glVertex3f(x, y, z);
            }
        }
        glEnd();
    }
}

static void torusFilled(int numc, int numt)
{
    int i, j, k;
    double s, t, x, y, z, twopi;
    float PI = 3.141592f;

    twopi = 2 * PI;
    for (i = 0; i < numc; i++)
    {
        glBegin(GL_TRIANGLE_STRIP);
        glColor3f(0.0f, 1.0f, 1.0f);
        for (j = 0; j <= numt; j++)
        {
            for (k = 1; k >= 0; k--)
            {
                s = (i + k) % numc + 0.5;
                t = j % numt;

                x = (1 + .1*cos(s*twopi / numc))*cos(t*twopi / numt);
                y = (1 + .1*cos(s*twopi / numc))*sin(t*twopi / numt);
                z = .1 * sin(s * twopi / numc);
                glVertex3f(x, y, z);
            }
        }
        glEnd();
    }
}

static void drawCylinder(int numMajor, int numMinor, float height, float radius)
	{

	float PI = 3.141592f;	

	double majorStep = height / numMajor;
	double minorStep = 2.0 * PI / numMinor;
	int i, j;

	for (i = 0; i < numMajor; ++i) {
	GLfloat z0 = 0.5 * height - i * majorStep;
	GLfloat z1 = z0 - majorStep;

	//GL_LINE_LOOP for wireframe
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINE_LOOP);
	for (j = 0; j <= numMinor; ++j) {
	double a = j * minorStep;
	GLfloat x = radius * cos(a);
	GLfloat y = radius * sin(a);
	glNormal3f(x / radius, y / radius, 0.0);
	glTexCoord2f(j / (GLfloat) numMinor, i / (GLfloat) numMajor);
	glVertex3f(x, y, z0);

	glNormal3f(x / radius, y / radius, 0.0);
	glTexCoord2f(j / (GLfloat) numMinor, (i + 1) / (GLfloat) numMajor);
	glVertex3f(x, y, z1);
	}
	glEnd();
	}
	}


static void drawCylinderFilled(int numMajor, int numMinor, float height, float radius)
	{

	float PI = 3.141592f;	

	double majorStep = height / numMajor;
	double minorStep = 2.0 * PI / numMinor;
	int i, j;

	for (i = 0; i < numMajor; ++i) {
	GLfloat z0 = 0.5 * height - i * majorStep;
	GLfloat z1 = z0 - majorStep;

	//GL_LINE_LOOP for wireframe
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUAD_STRIP);
	for (j = 0; j <= numMinor; ++j) {
	double a = j * minorStep;
	GLfloat x = radius * cos(a);
	GLfloat y = radius * sin(a);
	glNormal3f(x / radius, y / radius, 0.0);
	glTexCoord2f(j / (GLfloat) numMinor, i / (GLfloat) numMajor);
	glVertex3f(x, y, z0);

	glNormal3f(x / radius, y / radius, 0.0);
	glTexCoord2f(j / (GLfloat) numMinor, (i + 1) / (GLfloat) numMajor);
	glVertex3f(x, y, z1);
	}
	glEnd();
	}
	}

	#define DEG2RAD 3.14159/180.0
 
static void drawEllipse(float radiusX, float radiusY)
{
   int i;
 	
   glColor3f(0.0, 1.0, 1.0);
   glBegin(GL_LINE_LOOP);
 
   for(i=0;i<360;i++)
   {
      float rad = i*DEG2RAD;
      glVertex2f(cos(rad)*radiusX,
                  sin(rad)*radiusY);
   }
 
   glEnd();
}

static void drawEllipseFilled(float radiusX, float radiusY)
{
   int i;
 	
   glColor3f(0.0, 1.0, 1.0);
   glBegin(GL_LINE_STRIP);
 
   for(i=0;i<360;i++)
   {
      float rad = i*DEG2RAD;
      glVertex2f(cos(rad)*radiusX,
                  sin(rad)*radiusY);
   }
 
   glEnd();
}

GLvoid keyFunc(unsigned char key, int x, int y)
{

	choice = key;
	glutPostRedisplay();
	
}






void perspective(double fovY, double aspect, double zNear, double zFar)
{

	const double pi = 3.141592;
	double fW, fH;
	fH = tan( (fovY / 2) / 180 * pi ) * zNear;
	fH = tan( fovY / 360 * pi ) * zNear;
	fW = fH * aspect;
	glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

void DrawGrid(float minx, float maxx, float miny, float maxy, float step)
{
	float cx = minx;
	float cy = miny;
	glPushMatrix();
	  glTranslatef( 0.0, -2.0, -1.5);
	  glRotatef(90,1,0,0);
	  glLineWidth(1);
	  glColor3f(1, 0, 0);
	  glBegin(GL_LINES);
	     
	     while(cx <= maxx)
		 {
	        glVertex3f(cx, miny, 0);
			glVertex3f(cx, maxy, 0);
			cx += step;
		 }

		 while(cy <= maxy)
		 {
	        glVertex3f(minx, cy, 0);
			glVertex3f(maxx, cy, 0);
			cy += step;
		 }
	  glEnd();
	glPopMatrix();
}



/*  Initialize material property and light source.  */

void InitGL(void)
{
    // Light Properties
   /*GLfloat light_ambient[] = {0.0, 0.0, 0.0, 1.0};
    GLfloat light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};
    GLfloat mat_specular[] = {0.5, 0.5, 0.5, 1.0};     
    GLfloat mat_shininess[]= {30.0};      

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    // Material Properties
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);  
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess); 

    // Enable Various Components
    glEnable(GL_COLOR_MATERIAL);  
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);

    glColorMaterial(GL_FRONT,GL_DIFFUSE); 
	perspective(40.0, 1.0, 0.1, 120.0);//replaces gluPerspective
    //gluPerspective( 40.0, 1.0 , 0.1,  1.0);// replace
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();*/
}

/* Draw Objects */
void display(void)
{

	float lookAt[16];
	int countLook = 0;
	glm::mat4 currentLook;


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


//replace gluLookAt
    glGetFloatv(GL_MODELVIEW_MATRIX, lookAt);
    //store lookAt array into mat4 currentLook object
        for(int row = 0; row <4; row++)
        {
        	for(int col = 0; col< 4; col++)
        	{
        		currentLook[row][col] = lookAt[countLook];
        		countLook++;
        	}
        }
    currentLook = glm::lookAt(glm::vec3(0.0f, 0.0f, -10.0f),  glm::vec3(0.0f, 0.0f, 0.0f),  glm::vec3(0.0f, 1.0f, 0.0f));//Replaces gluLookAt
	const float *pSourceLook = (const float *)glm::value_ptr(currentLook);//create a pointer to store mat4 object into an array
        //store currentLook into lookAt array
        for(int i = 0; i < 16; ++i)
        {
        	lookAt[i] = pSourceLook[i];
        }
        glLoadMatrixf(lookAt);

        




switch(choice)
{
       case 'w':
       while(choice == 'w')
       {
	    if(isOn == true) 
	    {
	    //SPHERE
	    //glRotatef(45.0, 0.0, 1.0, 0.0);
	    glTranslatef(1.0, 1.0, 0.0);
	    glScalef(0.5, 0.5, 0.5);
        sphere(0.0, 0.0, 1.0);

        //TORUS
        glTranslatef(-2.0, -2.0, -8.0);
        torus(15, 50);

        //CYLINDER
        //glRotatef(45.0, 0.0, 1.0, 0.0);
        glTranslatef(-2.0, -2.0, 3.0);
        drawCylinder(10, 100, 5.0, 1.5);

        //ELLIPSE
        //glRotatef(45.0, 0.0, 1.0, 0.0);
        glTranslatef(2.0, 5.0, 0.0);
        drawEllipse(2.0, 1.0);
        isOn = false;
       break;
        }
	   else
	    {
	    //SPHERE FILLED
	    //glRotatef(45.0, 0.0, 1.0, 0.0);
	   	glTranslatef(1.0, 1.0, 0.0);
	   	glScalef(0.5, 0.5, 0.5);
        sphereFilled(0.0, 0.0, 1.0);

        //TORUS FILLED
        glTranslatef(-2.0, -2.0, -8.0);
        torusFilled(15, 50);

        //CYLINDER FILLED
        //glRotatef(45.0, 0.0, 1.0, 0.0);
        glTranslatef(-2.0, -2.0, 3.0);
        drawCylinderFilled(10, 100, 5.0, 1.5);

        //ELLIPSE FILLED
        glTranslatef(2.0, 5.0, 0.0);
        drawEllipseFilled(2.0, 1.0);
        isOn = true;
        break;
 		}}

 		case 'x':

 		while(choice == 'x')
 		{
 		glRotatef(45.0, 1.0, 0.0, 0.0);
 		//SPHERE
	 
	    glTranslatef(1.0, 1.0, 0.0);
	    glScalef(0.5, 0.5, 0.5);
        sphere(0.0, 0.0, 1.0);

        //TORUS
        glRotatef(45.0, 1.0, 0.0, 0.0);
        glTranslatef(-2.0, -2.0, 0.0);
        torus(15, 50);

        //CYLINDER
        
        glRotatef(45.0, 1.0, 0.0, 0.0);
        glTranslatef(-4.0, -4.0, 0.0);
        drawCylinder(10, 100, 5.0, 1.5);

        //ELLIPSE
        glRotatef(45.0, 1.0, 0.0, 0.0);
        glTranslatef(2.0, 5.0, 0.0);
        drawEllipse(2.0, 1.0);
 		break;
 		}

 		case 'y':
 		while(choice == 'y')
 		{
 		
 		//SPHERE
	    glRotatef(45.0, 0.0, 1.0, 0.0);
	    glTranslatef(1.0, 1.0, 0.0);
	    glScalef(0.5, 0.5, 0.5);
        sphere(0.0, 0.0, 1.0);

        //TORUS
        glRotatef(45.0, 0.0, 1.0, 0.0);
        glTranslatef(-2.0, -2.0, 0.0);
        torus(15, 50);

        //CYLINDER
        glRotatef(45.0, 0.0, 1.0, 0.0);
        glTranslatef(-4.0, -4.0, 0.0);
        drawCylinder(10, 100, 5.0, 1.5);

        //ELLIPSE
        glRotatef(45.0, 0.0, 1.0, 0.0);
        //glTranslatef(5.0, 5.0, 0.0);
        drawEllipse(2.0, 1.0);
 		break;
 		}

 		case 'z':
 		while(choice == 'z')
 		{
 		//SPHERE
	    glRotatef(45.0, 0.0, 0.0, 1.0);
	    glTranslatef(1.0, 1.0, 0.0);
	    glScalef(0.5, 0.5, 0.5);
        sphere(0.0, 0.0, 1.0);

        //TORUS
        glRotatef(45.0, 0.0, 0.0, 1.0);
        glTranslatef(-2.0, -2.0, -8.0);
        torus(15, 50);

        //CYLINDER
        glRotatef(45.0, 0.0, 0.0, 1.0);
        glTranslatef(-2.0, -2.0, 3.0);
        drawCylinder(10, 100, 5.0, 1.5);

        //ELLIPSE
        glRotatef(45.0, 0.0, 0.0, 1.0);
        glTranslatef(2.0, 5.0, 0.0);
        drawEllipse(2.0, 1.0);
 		break;
 		}




}      



    glPopMatrix();

	//DrawGrid(-10, 10, -10, 10, 1);
    glutSwapBuffers();
}

void myReshape(int w, int h) 
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    gluPerspective( 40.0, (GLfloat) h / (GLfloat) w , 0.1,  120.0); //replace.
    glMatrixMode(GL_MODELVIEW);

}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowPosition(50, 50);
    glutInitWindowSize(700, 700);
    glutCreateWindow("3D Scene");
    InitGL();
    glutReshapeFunc(myReshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyFunc);
    glutMainLoop();
    return 0;
}

